<?php

namespace Wcs\SheperdBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Wcs\SheperdBundle\Entity\Sheep;

class LoadSheepData implements FixtureInterface
{
/**
* {@inheritDoc}
*/
public function load(ObjectManager $manager)
{
        $sheep1 = new Sheep ();
        $sheep1 -> setName ('George');
        $sheep1 -> setAge (8);
        $sheep1 -> setColor ('Jaune');
        $sheep1 -> setGender ('m');
        $sheep1 -> setNumero (1);
        $sheep1 -> setRace ('Dalamtien');
        $manager -> persist($sheep1);

        $sheep2 = new Sheep ();
        $sheep2 -> setName ('George');
        $sheep2 -> setAge (8);
        $sheep2 -> setColor ('Jaune');
        $sheep2 -> setGender ('m');
        $sheep2 -> setNumero (1);
        $sheep2 -> setRace ('Dalamtien');
        $manager -> persist($sheep2);

        $sheep3 = new Sheep ();
        $sheep3 -> setName ('George');
        $sheep3 -> setAge (8);
        $sheep3 -> setColor ('Jaune');
        $sheep3 -> setGender ('m');
        $sheep3 -> setNumero (1);
        $sheep3 -> setRace ('Dalamtien');
        $manager -> persist($sheep3);

        $manager->flush();
}
}