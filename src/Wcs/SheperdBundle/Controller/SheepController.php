<?php

namespace Wcs\SheperdBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Wcs\SheperdBundle\Entity\Sheep;
use Wcs\SheperdBundle\Form\SheepType;

/**
 * Sheep controller.
 *
 */
class SheepController extends Controller
{

    /**
     * Lists all Sheep entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WcsSheperdBundle:Sheep')->findAll();

        return $this->render('WcsSheperdBundle:Sheep:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Sheep entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Sheep();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('sheep_show', array('id' => $entity->getId())));
        }

        return $this->render('WcsSheperdBundle:Sheep:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Sheep entity.
     *
     * @param Sheep $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Sheep $entity)
    {
        $form = $this->createForm(new SheepType(), $entity, array(
            'action' => $this->generateUrl('sheep_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Sheep entity.
     *
     */
    public function newAction()
    {
        $entity = new Sheep();
        $form   = $this->createCreateForm($entity);

        return $this->render('WcsSheperdBundle:Sheep:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Sheep entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WcsSheperdBundle:Sheep')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sheep entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('WcsSheperdBundle:Sheep:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Sheep entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WcsSheperdBundle:Sheep')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sheep entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('WcsSheperdBundle:Sheep:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Sheep entity.
    *
    * @param Sheep $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Sheep $entity)
    {
        $form = $this->createForm(new SheepType(), $entity, array(
            'action' => $this->generateUrl('sheep_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Sheep entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WcsSheperdBundle:Sheep')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sheep entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('sheep_edit', array('id' => $id)));
        }

        return $this->render('WcsSheperdBundle:Sheep:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Sheep entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WcsSheperdBundle:Sheep')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sheep entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sheep'));
    }

    /**
     * Creates a form to delete a Sheep entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sheep_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
