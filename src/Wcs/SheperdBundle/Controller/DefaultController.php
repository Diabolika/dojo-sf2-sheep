<?php

namespace Wcs\SheperdBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('WcsSheperdBundle:Default:index.html.twig', array('name' => $name));
    }
}
