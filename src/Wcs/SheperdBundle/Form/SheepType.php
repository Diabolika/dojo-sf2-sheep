<?php

namespace Wcs\SheperdBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SheepType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('age')
            ->add('color')
            ->add('name')
            ->add('numero')
            ->add('race')
            ->add('gender', 'choice', array(
            'choices' => array('m' => 'Masculin', 'f' => 'Féminin'),
            'required' => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wcs\SheperdBundle\Entity\Sheep'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wcs_sheperdbundle_sheep';
    }
}
